﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchTry : MonoBehaviour
{
    public Light lighting;
    public ParticleSystem fire;
    public ParticleSystem flames;
    public ParticleSystem glow;
    public ParticleSystem smokeDark;
    public ParticleSystem smokeLit;
    public ParticleSystem sparksRising;

    private void Start()
    {
        lighting.enabled = false;

        var emission = fire.emission;
        emission.enabled = false;

        emission = flames.emission;
        emission.enabled = false;

        emission = glow.emission;
        emission.enabled = false;

        emission = smokeDark.emission;
        emission.enabled = false;

        emission = smokeLit.emission;
        emission.enabled = false;

        emission = sparksRising.emission;
        emission.enabled = false;
    }
}
