﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIMainMenu : UIScene
{
    public string nextScene;

    void Start()
    {

    }
    public void StartGame()
    {
        SceneManager.LoadScene(nextScene);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    public void ShowCredits()
    {
        SceneManager.LoadScene("Credits");
    }
}
