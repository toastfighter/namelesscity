﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundTrigger : MonoBehaviour
{
    public AudioSource audioPlayer;

    [SerializeField]
    private string soundTriggerObject;

    private void Awake()
    {
        audioPlayer = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            var playlist = FindObjectOfType<Playlist>();

            if (soundTriggerObject == "Tinnitus")
            {
                playlist.Tinnitus();
                Debug.Log("Entered.");
            }

            if (soundTriggerObject == "Voice1")
            {
                playlist.Voice1();
                Debug.Log("Entered.");
            }

            if (soundTriggerObject == "Voice2")
            {
                playlist.Voice2();
                Debug.Log("Entered.");
            }

            if (soundTriggerObject == "Torch")
            {
                playlist.Torch();
                Debug.Log("Entered.");
            }

            if (soundTriggerObject == "Desert2Outro")
            {
                playlist.Desert2Outro();
                Debug.Log("Entered.");
            }

            if (soundTriggerObject == "Desert2FadeOut")
            {
                playlist.Desert2FadeOut();
                Debug.Log("Entered.");
            }

            if (soundTriggerObject == "Dungeon")
            {
                playlist.Dungeon();
                Debug.Log("Entered.");
            }

            if (soundTriggerObject == "DungeonNoVocals")
            {
                playlist.DungeonNoVocals();
                Debug.Log("Entered.");
            }
            if (soundTriggerObject == "Growl1")
            {
                playlist.Growl1();
                Debug.Log("Entered.");
            }

            if (soundTriggerObject == "Growl2")
            {
                playlist.Growl2();
                Debug.Log("Entered.");
            }

            if (soundTriggerObject == "Growl3")
            {
                playlist.Growl3();
                Debug.Log("Entered.");
            }

            if (soundTriggerObject == "Mmh")
            {
                playlist.Mmh();
                Debug.Log("Entered.");
            }

            if (soundTriggerObject == "Gurgel")
            {
                playlist.Gurgel();
                Debug.Log("Entered.");
            }
        }
    }
}
