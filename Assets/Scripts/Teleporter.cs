﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Teleporter : MonoBehaviour
{
    public GameObject player;
    public Transform destination;
    private FirstPersonController firstPerson;

    private void OnTriggerEnter(Collider other)
    {
       if(other.tag == "Player")
        {
            firstPerson.Teleport(destination.position, destination.rotation);
        }
    }

    private void Awake()
    {
        firstPerson = FindObjectOfType<FirstPersonController>();
    }

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}
}
