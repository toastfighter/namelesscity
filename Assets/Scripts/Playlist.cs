﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playlist : MonoBehaviour
{
    public AudioSource audioSource;

    [SerializeField]
    private AudioClip voice1;
    [SerializeField]
    private AudioClip voice2;
    //[SerializeField]
    //private AudioClip walkOnSand;
    //[SerializeField]
    //private AudioClip walkOnStone;
    [SerializeField]
    private AudioClip desert2Outro;
    [SerializeField]
    private AudioClip desert2FadeOut;
    [SerializeField]
    private AudioClip torch;
    [SerializeField]
    private AudioClip dungeon;
    [SerializeField]
    private AudioClip dungeonNoVocals;
    [SerializeField]
    private AudioClip growl1;
    [SerializeField]
    private AudioClip growl2;
    [SerializeField]
    private AudioClip growl3;
    [SerializeField]
    private AudioClip mmh;
    [SerializeField]
    private AudioClip gurgel;
    [SerializeField]
    private AudioClip tinnitus;

    public void Awake()
    {
        var audioSource = GetComponent<AudioSource>();
    }
    public void Voice1 ()
    {
        audioSource.clip = voice1;
        audioSource.Play();
	}
    public void Voice2()
    {
        audioSource.clip = voice2;
        audioSource.Play();
    }
    //public void WalkOnSand()
    //{
    //    audioSource.clip = walkOnSand;
    //    audioSource.Play();
    //}
    //public void WalkOnStone()
    //{
    //    audioSource.clip = walkOnStone;
    //    audioSource.Play();
    //}
    public void Desert2Outro()
    {
        audioSource.clip = desert2Outro;
        audioSource.Play();
    }
    public void Desert2FadeOut()
    {
        audioSource.clip = desert2FadeOut;
        audioSource.Play();
    }
    public void Torch()
    {
        audioSource.clip = torch;
        audioSource.Play();
    }
    public void Dungeon()
    {
        audioSource.clip = dungeon;
        audioSource.Play();
    }
    public void DungeonNoVocals()
    {
        audioSource.clip = dungeonNoVocals;
        audioSource.Play();
    }
    public void Growl1()
    {
        audioSource.clip = growl1;
        audioSource.Play();
    }
    public void Growl2()
    {
        audioSource.clip = growl2;
        audioSource.Play();
    }
    public void Growl3()
    {
        audioSource.clip = growl3;
        audioSource.Play();
    }
    public void Mmh()
    {
        audioSource.clip = mmh;
        audioSource.Play();
    }
    public void Gurgel()
    {
        audioSource.clip = gurgel;
        audioSource.Play();
    }
    public void Tinnitus()
    {
        audioSource.clip = tinnitus;
        audioSource.Play();
    }
    public void TorchStop()
    {
        audioSource.clip = torch;
        audioSource.Stop();
    }
}
