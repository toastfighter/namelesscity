﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateStairs : MonoBehaviour
{
    [SerializeField]
    public GameObject Appear;

    [SerializeField]
    public GameObject Disappear;

    private void OnTriggerEnter(Collider other)
    {
        Appear.SetActive(true);
        Disappear.SetActive(false);
    }
}
