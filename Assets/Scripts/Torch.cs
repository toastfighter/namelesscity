﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class Torch : MonoBehaviour
{
    [SerializeField]
    private Light lighting;

    // Todo: Has to become an array of particle system

    [SerializeField]
    private ParticleSystem[] particles;

    private void Awake()
    {
        // Todo: Use GetComponentsInChildren to get all particles and save in array
        particles = GetComponentsInChildren<ParticleSystem>();
        lighting = GetComponentInChildren<Light>();

        lighting.enabled = false;

        // Todo: Iterate over particle array to Stop() all particles at Awake
        foreach (ParticleSystem system in particles)
        {
            system.enableEmission = false;
        }
    }

    public void Light()
    {
        lighting.enabled = true;

        // Todo: Iterate over particle array to Play() all particles at Light
        foreach (ParticleSystem system in particles)
        {
            system.enableEmission = true;
        }
    }

    public void LightOut()
    {
        lighting.enabled = false;

        foreach (ParticleSystem system in particles)
        {
            system.enableEmission = false;
        }
    }
}
