﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseTorch2 : MonoBehaviour
{
    private GameObject hitObject;
    private TorchTry torch;

    void Update()
    {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 3f, LayerMask.GetMask("Torch")))
        {
            print("Looking at " + hit.transform.name);

            if (hit.transform.gameObject.GetComponent("TorchTry"))
            {
                hitObject = hit.transform.gameObject;

                torch = hitObject.GetComponent<TorchTry>();

                if (Input.GetButtonDown("Fire1") && !torch.lighting.enabled)
                {
                    torch.lighting.enabled = true;
                    Debug.Log("fire");
                    torch.lighting.enabled = true;

                    var emission = torch.fire.emission;
                    emission.enabled = true;

                    emission = torch.flames.emission;
                    emission.enabled = true;

                    emission = torch.glow.emission;
                    emission.enabled = true;

                    emission = torch.smokeDark.emission;
                    emission.enabled = true;

                    emission = torch.smokeLit.emission;
                    emission.enabled = true;

                    emission = torch.sparksRising.emission;
                    emission.enabled = true;
                }
                else if (Input.GetButtonDown("fire1") && torch.lighting.enabled)
                {
                    Debug.Log("fire");
                    torch.lighting.enabled = false;

                    var emission = torch.fire.emission;
                    emission.enabled = false;

                    emission = torch.flames.emission;
                    emission.enabled = false;

                    emission = torch.glow.emission;
                    emission.enabled = false;

                    emission = torch.smokeDark.emission;
                    emission.enabled = false;

                    emission = torch.smokeLit.emission;
                    emission.enabled = false;

                    emission = torch.sparksRising.emission;
                    emission.enabled = false;
                }
            }
        }
    }
}
