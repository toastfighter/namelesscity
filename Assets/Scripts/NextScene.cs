﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextScene : MonoBehaviour
{
    

    public string nextScene;

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //var Fade = FindObjectOfType<CameraFade>();
            //StartCoroutine(Fade.FadeIn());


            SceneManager.LoadScene(nextScene);
        }
    }
    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}
}
