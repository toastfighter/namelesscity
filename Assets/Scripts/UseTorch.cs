﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseTorch : MonoBehaviour
{
    private GameObject hitObject;
    private Torch torch;
    private bool isLight;
    [SerializeField]
    private AudioSource audioPlayer;

    private void Awake()
    {
        isLight = false;
        audioPlayer = GetComponent<AudioSource>();
    }
    void Update ()
    {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 3f, LayerMask.GetMask("Torch")))
        {
            print("Looking at " + hit.transform.name);

            if (hit.transform.gameObject.GetComponent("Torch"))
            {
                hitObject = hit.transform.gameObject;

                torch = hitObject.GetComponent<Torch>();

                var playlist = FindObjectOfType<Playlist>();

                if (Input.GetButtonDown("Fire1") && !isLight)
                {
                    torch.Light();
                    playlist.Torch();
                    isLight = true;
                }
                else if (Input.GetButtonDown("Fire1") && isLight)
                {
                    torch.LightOut();
                    playlist.TorchStop();
                    isLight = false;
                }
            }
        }
    }
}
