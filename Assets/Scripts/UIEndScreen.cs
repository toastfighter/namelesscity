﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIEndScreen : UIScene
{
    public string nextScene;

    public void Awake()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

    }

    void Start()
    {

    }
    public void BackToMenu()
    {
        SceneManager.LoadScene(nextScene);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    public void ShowCredits()
    {

    }
}
